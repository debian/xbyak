xbyak (7.22-1) unstable; urgency=low

  * New upstream version 7.22

 -- Andrea Pappacoda <andrea@pappacoda.it>  Tue, 14 Jan 2025 23:22:26 +0100

xbyak (7.07.1-1) unstable; urgency=medium

  * New upstream version 7.07.1
    - Closes: #1080144
  * First dgit upload
  * d/README.source: document packaging workflow
  * d/control: declare dpkg-build-api = 1
  * d/control: apply X-Style: black

 -- Andrea Pappacoda <andrea@pappacoda.it>  Mon, 02 Sep 2024 09:49:39 +0200

xbyak (7.05-1) unstable; urgency=medium

  * New upstream version 7.05

 -- Andrea Pappacoda <andrea@pappacoda.it>  Fri, 12 Jan 2024 10:01:26 +0100

xbyak (7.02-1) unstable; urgency=medium

  * New upstream version 7.02
  * Upload to unstable
  * d/patches: drop upstreamed patches

 -- Andrea Pappacoda <andrea@pappacoda.it>  Sun, 24 Dec 2023 13:13:15 +0100

xbyak (7.00-1) experimental; urgency=medium

  * New upstream version 7.00
  * d/patches: fix tests on 32 bit systems

 -- Andrea Pappacoda <andrea@pappacoda.it>  Mon, 11 Dec 2023 19:49:37 +0100

xbyak (6.73-1) unstable; urgency=medium

  * New upstream version 6.73
    - Huge pages support
    - Add sha512/sm3/sm4/avx-vnni-int16
    - Add xbegin/xabort/xend

 -- Andrea Pappacoda <andrea@pappacoda.it>  Tue, 12 Sep 2023 15:32:15 +0200

xbyak (6.70-1) unstable; urgency=medium

  * New upstream version 6.70

 -- Andrea Pappacoda <andrea@pappacoda.it>  Tue, 11 Jul 2023 17:03:25 +0200

xbyak (6.69.2-1) unstable; urgency=medium

  * New upstream version 6.69.2
  * Upload to unstable

 -- Andrea Pappacoda <andrea@pappacoda.it>  Tue, 04 Jul 2023 15:11:18 +0200

xbyak (6.69.1-1) experimental; urgency=medium

  * New upstream version 6.69.1

 -- Andrea Pappacoda <andrea@pappacoda.it>  Mon, 22 May 2023 01:18:41 +0200

xbyak (6.68-1) unstable; urgency=medium

  * New upstream version 6.68

 -- Andrea Pappacoda <andrea@pappacoda.it>  Wed, 14 Dec 2022 18:24:27 +0100

xbyak (6.67-1) unstable; urgency=medium

  * New upstream version 6.67

 -- Andrea Pappacoda <andrea@pappacoda.it>  Thu, 01 Dec 2022 20:04:53 +0100

xbyak (6.66-1) unstable; urgency=medium

  * New upstream version 6.66

 -- Andrea Pappacoda <andrea@pappacoda.it>  Mon, 28 Nov 2022 15:24:52 +0100

xbyak (6.65-1) unstable; urgency=medium

  * New upstream version 6.65
  * d/patches: drop upstreamed patches
  * d/rules: use DEB_HOST_ARCH_BITS for pointer size

 -- Andrea Pappacoda <andrea@pappacoda.it>  Thu, 24 Nov 2022 16:23:34 +0100

xbyak (6.64-2) unstable; urgency=medium

  * d/patches: x32 support

 -- Andrea Pappacoda <andrea@pappacoda.it>  Sun, 20 Nov 2022 15:26:15 +0100

xbyak (6.64-1) unstable; urgency=medium

  * New upstream version 6.64

 -- Andrea Pappacoda <andrea@pappacoda.it>  Sun, 06 Nov 2022 22:04:09 +0100

xbyak (6.63-1) unstable; urgency=medium

  * New upstream version 6.63

 -- Andrea Pappacoda <andrea@pappacoda.it>  Sun, 09 Oct 2022 13:16:09 +0200

xbyak (6.62-1) unstable; urgency=medium

  * New upstream version 6.62
  * d/rules: use execute_after_*

 -- Andrea Pappacoda <andrea@pappacoda.it>  Sat, 17 Sep 2022 14:10:45 +0200

xbyak (6.61.2-1) unstable; urgency=medium

  * New upstream version 6.61.2

 -- Andrea Pappacoda <andrea@pappacoda.it>  Fri, 19 Aug 2022 00:56:44 +0200

xbyak (6.60.2-1) unstable; urgency=medium

  * New upstream version 6.60.2

 -- Andrea Pappacoda <andrea@pappacoda.it>  Thu, 16 Jun 2022 23:30:22 +0200

xbyak (6.052-1) unstable; urgency=medium

  * New upstream version 6.052
  * d/upstream/metadata: update paths

 -- Andrea Pappacoda <andrea@pappacoda.it>  Thu, 26 May 2022 13:22:26 +0200

xbyak (6.04-1) unstable; urgency=medium

  * New upstream version 6.04
  * d/patches: drop upstreamed patches

 -- Andrea Pappacoda <andrea@pappacoda.it>  Mon, 18 Apr 2022 21:02:22 +0200

xbyak (6.03-1) unstable; urgency=medium

  * New upstream version 6.03
  * d/{control,rules}: build examples and run tests
  * d/tests: add autopkgtests

 -- Andrea Pappacoda <andrea@pappacoda.it>  Sun, 13 Mar 2022 00:48:30 +0100

xbyak (6.02-1) unstable; urgency=medium

  * New upstream version 6.02

 -- Andrea Pappacoda <andrea@pappacoda.it>  Sun, 13 Feb 2022 22:40:01 +0100

xbyak (6.01-1) unstable; urgency=medium

  * New upstream version 6.01
  * d/control: add Vcs fields
  * d/patches: drop upstreamed patches

 -- Andrea Pappacoda <andrea@pappacoda.it>  Wed, 12 Jan 2022 23:11:02 +0100

xbyak (6.00-2) unstable; urgency=medium

  * Backport b29e471 (CMake Config file include path fix)
  * d/control: remove Vcs-* fields, as no vcs is used
  * d/upstream/metadata: update Documentation and Changelog fields
  * d/watch: use /tags instead of /releases

 -- Andrea Pappacoda <andrea@pappacoda.it>  Sat, 20 Nov 2021 22:04:30 +0100

xbyak (6.00-1) unstable; urgency=low

  * Initial release. Closes: #995760

 -- Andrea Pappacoda <andrea@pappacoda.it>  Tue, 05 Oct 2021 10:29:03 +0200
